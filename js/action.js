// let task_1 = {


// };// объект , поля свойства    ключ: значение





let tasks = [];//массив [] объектов;

getTasks();//get -получить, item -элемент



function drawTask(task) {//фунцкция формирования объекта, в task на каждой ++ будет лежать объект
    return `
     <div class="task">                
<input type="checkbox" class="task__complete" ${task.isComplete ? 'checked' : ''}>
<span class="task__number">${task.id}</span>
<span class="task__name">${task.name}</span>
</div>`;
}



const list = document.querySelector('.container__list');//обращение к html и выводит на экран 

// list.innerHTML = drawTask(tasks[0]);//функция вызова объекта
// list.innerHTML += drawTask(tasks[1]);//функция вызова объекта
// list.innerHTML += drawTask(tasks[2]);//функция вызова объекта
// ;//функция вызова объекта


// tasks.forEach(item => {
//     list.innerHTML += drawTask(item)
// });




// console.log(tasks[1].name);


//////////////////////////////////////////

function createTaskTag(task) {//делаем разметку
    const name = document.createElement('span');//генерирует тег
    name.className = 'task__name task__name_active'; //span class task__name
    name.innerText = task.name;//'тренирока'


    const numder = document.createElement('span');
    numder.className = 'task_number';
    numder.innerText = task.id;

    const checkbox = document.createElement('input');//по умолчанию тип text
    checkbox.type = 'checkbox';
    checkbox.className = 'task__complete';
    checkbox.checked = task.isComplete;


    const deleteBtn = document.createElement('div');
    deleteBtn.className = 'task__delete';
    deleteBtn.innerText = 'X';//div class "task__delete">x</div>
    deleteBtn.id = 'task_' + task.id;
    deleteBtn.addEventListener('click', itemClick);


    const editBtn = document.createElement('div');
    editBtn.className = 'edit';
    editBtn.innerText = 'E';
    editBtn.id = 'task_' + task.id;
    editBtn.addEventListener('click', toggleEdit); 


    const editInput = document.createElement('input');
    editInput.className = 'task__edit';
    editInput.value = task.name;




    const taskTag = document.createElement('div');
    taskTag.className = 'task';//div class task
    taskTag.appendChild(checkbox);//функция вкладывет теги в другие теги, class checkbox
    taskTag.appendChild(numder);//imput number
    taskTag.appendChild(name);//name
    taskTag.appendChild(editInput);//добавили тег в разметку
    taskTag.appendChild(editBtn);//доп кнопка
    taskTag.appendChild(deleteBtn);


    return taskTag;
}



drawTasks();


function drawTasks() {
    list.innerHTML = '';
    tasks.forEach(item => {  //перерисовка
        const myTask = createTaskTag(item);
        list.appendChild(myTask);
        // list.appendChild(createTaskTag(item));
    });
}






//////////////////



////-------- Events


const button = document.querySelector('#sayHello');//обращение к id или селектору


function itemClick(event) {//event- объект с информацией по событию, таргет
    const taskIdString = event.target.id;//объект, split('разделиель')-> превращает в строку в массив по разделителю(сиМвол), строка поля id,таргет по имени котрое ищешь
    const taskId = Number(taskIdString.split('_')[1]);//разделитель,строку в массив по разделителю,('разделитель'), +преобразовывает к числу(Number)
    //    console.log(taskId);
    /* const task = tasks.find(item => item.id === taskId);//сравнение? ищет элемент по совпадению(элем с которой сравнивают === что сравнивает)
     const index = tasks.indexOf(task);//ищет индекс
     tasks.splice(index, 1)//индекс удаляемого элем. колл. удаляемых элементов
     console.log(tasks);*/
    //перерисовать массив данных
    tasks = tasks.filter(item => item.id !== Number(taskId));
    drawTasks();
    storageChange();
    // const taskString = JSON.stringify(tasks);//преобразовали в джийсон
    // localStorage.setItem('tasks', taskString);
    //самый! лучший! день .split('')=> ['самый' 'лучший' 'день']
    //    button.removeEventListener('click', itemClick);//отписка от события
}



// button.addEventListener('click', itemClick);//1параметр название события, 2й колбэк функция, подпись на событие

//  console.log(button);



// const arr = [ 2,3,5,7,8];
// arr.indexOf(1); = 3 индекс -1 ничего не нашел


const addInput = document.querySelector('#add');
addInput.addEventListener('click', addTask);

const input = document.querySelector('#taskName');
input.addEventListener('keydown', checkEnterKey)

function checkEnterKey(event) {
    if (event.key === 'Enter') {
        addTask();
    }

}

document.addEventListener('keydown', checkDeleteKey);

function checkDeleteKey(event) {
    if (event.key === 'Delete') {
        deleteLastTask();
    }
}


function deleteLastTask() {
    tasks.pop();//выдергивает последний элемент, shift первый
    storageChange();//перезаписывыем массив
    drawTasks();//перерисовка
}



function addTask() {//добовление
    const input = document.querySelector('#taskName');
    const taskName = input.value;
    // const taskName = 'Отдохнуть';//забрали из импут задачу
    const newTask = {
        isComplete: false,
        name: taskName,
        id: getNewId()// max id +1
    };
    tasks.push(newTask);
    input.value = '';
    drawTasks();

    storageChange();

    input.focus();

}


function storageChange() {
    const newTaskJson = JSON.stringify(tasks);//преобразовали в стороку
    localStorage.setItem('tasks', newTaskJson);//отправили в локал Сторидж(хранилище)
}
//1. забрать из локал Сторидж содеожимое по нашему ключу()джейсон стринг
//2. пробразовать(распарсить )строку метод парсе
//3. Добавитьв в преобразованный массив новую таску(задачу)
//4. Перобразовать полученый массив в джейсон строку джсон .стрингифайн
//5. отправить этот массив в локалСторидж 






function getNewId() {
    //     let max=0;
    //    tasks.forEach(item => {
    //    if(item.id > max){
    //        max = item.id+1
    //    }
    //    });

    const ids = tasks.map(item => item.id);
    if (ids.length > 0) {
        const max = Math.max(...ids);
        return max + 1;
    }

    return 0;

}
console.log(getNewId());



const addButton = document.querySelector('#add');
addButton.addEventListener('click', addTask);



////////////////////////////////////////////////



function getTasks() {
    const tasksStorage = localStorage.getItem('tasks');//обращение к хранилищу браузера
    // console.log(tasksStorage);
    if (tasksStorage) {
        tasks = JSON.parse(tasksStorage);
    } else {
        tasks = [];
    }
}



function toggleEdit(event) {
    const target = event.target;
    const classList = target.className;//ищем  имя класса
    const isHaveActive = classList.includes('active');//ищем состояние класса
    const taskName = target.parentNode.children[2];
    const editInput = target.parentNode.children[3];
    if(isHaveActive) {//если есть такой класс удаляем
        target.classList.remove('edit_active');//добовдяем класс

        taskName.classList.add('task__name_active');//добовляем класс

        editInput.classList.remove('task__edit_active');//удаляем модификатор

        const taskIdString = target.id;//берем ай ди

        const taskId = Number(taskIdString.split('_')[1]);//превращвем в массив по разделителю

        const currentTask = tasks.find(item => item.id === taskId);//сравниваем ай ди

        console.log(taskId);

        currentTask.name = editInput.value;//сохраняем новое введенное значение

        drawTasks();//перерисовка
        storageChange();
    }else { //если нету добавляем
        target.classList.add('edit_active');
        taskName.classList.remove('task__name_active');
        editInput.classList.add('task__edit_active');
    }



   console.log(taskName, editInput);
}